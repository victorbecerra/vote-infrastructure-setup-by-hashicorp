terraform {
  required_version = ">=0.12"
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "TFCLOUD_ORGANIZATION"

    workspaces {
      name = "TFCLOUD_WORKSPACE"
    }
  }
}

module "vault" {
  source                      = "app.terraform.io/TFCLOUD_ORGANIZATION/vaulthelm/google"
  version                     = "0.10.0"
  project                     = var.project
  region                      = var.region
  kubernetes_cluster          = var.kubernetes_cluster
  kubernetes_cluster_location = var.kubernetes_cluster_location
  namespace                   = var.namespace
}
